package com.secondstack.training.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Latief
 * Date: 1/8/13
 * Time: 6:09 AM
 */
public class ServletToJsp extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            getServletConfig().getServletContext().getRequestDispatcher(
                    "/WEB-INF/view/hello.jsp").forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
