package com.secondstack.training.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: Latief
 * Date: 1/8/13
 * Time: 5:34 AM
 */
public class HelloWorld extends HttpServlet {
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        pw.println("<html>");
        pw.println("<head><title>Hello World</title></head>");
        pw.println("<body>");
        pw.println("<h1>Hello World</h1>");
        pw.println("<p><a href=\"/ServletToJSPUrl\">Servlet to JSP Example</a></p>" +
                "<p><a href=\"/PostServletUrl\">Form Example</a></p>" +
                "<p><a href=\"/BootstrapUrl\">Bootstrap Example</a></p>" +
                "<p><a href=\"/ImageUrl\">Image Example</a></p>");
        pw.println("</body></html>");
    }
}
