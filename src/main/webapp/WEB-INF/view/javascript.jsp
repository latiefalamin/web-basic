<!DOCTYPE html>
<%--
  Created by IntelliJ IDEA.
  User: Latief
  Date: 1/9/13
  Time: 10:09 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap.min.css">
    <script type="text/javascript" src="/resources/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/resources/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="/resources/bootstrap/js/bootstrap.min.js"></script>
    <style type="text/css">

            /* Customize the navbar links to be fill the entire space of the .navbar */
        .navbar .navbar-inner {
            padding: 0;
        }

        .navbar .nav {
            margin: 0;
            display: table;
            width: 100%;
        }

        .navbar .nav li {
            display: table-cell;
            width: 1%;
            float: none;
        }

        .navbar .nav li a {
            font-weight: bold;
            text-align: center;
            border-left: 1px solid rgba(255, 255, 255, .75);
            border-right: 1px solid rgba(0, 0, 0, .1);
        }

        .navbar .nav li:first-child a {
            border-left: 0;
            border-radius: 3px 0 0 3px;
        }

        .navbar .nav li:last-child a {
            border-right: 0;
            border-radius: 0 3px 3px 0;
        }
    </style>

    <script type="text/javascript">

        //fungsi untuk menambahkan even pada button yang menampilkan modal
        function addButtonShowModalEvent(){
            //menampilkan modal
            $("a.btn").click(function () {
                $('#myModal').modal('toggle');
            });
        }

        $(document).ready(function () {

            //tambahkan even menampilkan modal pada button tersebut
            addButtonShowModalEvent();

            //toggle navigasi yang active
            function toggleNavbar(menu) {
                $('ul.nav').children().removeClass('active');
                menu.addClass('active');
            }

            //action menu
            $("#menuHome").click(function () {
                toggleNavbar($(this));
                var content = "<h1>Mari Belajar Web Programming!</h1>" +
                        "<p>Kita belajar mendisain halaman dengan Bootstrap serta belajar action evennya dengan JavaScript dan JQuery.</p>" +
                        "<p><a role='button' data-toggle='modal' class='btn btn-primary btn-large'>Tampilkan Modal &raquo;</a></p>"
                $('.hero-unit').html(content);
                addButtonShowModalEvent();
            });
            $("#menuPinguin").click(function () {
                toggleNavbar($(this));
                var content = "<h2>Pinguin</h2>" +
                        "<img src='/resources/images/Penguins.jpg' class='img-circle'>Mamalia yang bisa berenang</img>";
                $('.hero-unit').html(content);
            });
            $("#menuTulip").click(function () {
                toggleNavbar($(this));
                var content = "<h2>Tulip</h2>" +
                        "<img src='/resources/images/Tulips.jpg' class='img-polaroid'>Bunga yang indah</img>";
                $('.hero-unit').html(content);
            });
            $("#menuKoala").click(function () {
                toggleNavbar($(this));
                var content = "<h2>Koala</h2>" +
                        "<img src='/resources/images/Koala.jpg' class='img-circle'>Suka memanjat tapi sangat lambat</img>";
                $('.hero-unit').html(content);
            });
            $("#menuJellyfish").click(function () {
                toggleNavbar($(this));
                var content = "<h2>Ubur ubur</h2>" +
                        "<img src='/resources/images/Jellyfish.jpg' class='img-polaroid'>Awas sengatannya mematikan</img>";
                $('.hero-unit').html(content);
            });
            $("#menuDesert").click(function () {
                toggleNavbar($(this));
                var content = "<h2>Gurun</h2>" +
                        "<img src='/resources/images/Desert.jpg' class='img-polaroid'>Panas terik matahari, langka hujan dan air</img>";
                $('.hero-unit').html(content);
            });
        });
    </script>

    <title>Bootstrap</title>
</head>
<body>

<div class="container">

    <div class="masthead">
        <h3 class="muted">2ndStack Bootstrap & JavaScript Training</h3>

        <%--navbar--%>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <ul class="nav">
                        <li id="menuHome" class="active"><a href="#">Home</a></li>
                        <li id="menuPinguin"><a href="#">Pinguin</a></li>
                        <li id="menuTulip"><a href="#">Tulip</a></li>
                        <li id="menuKoala"><a href="#">Koala</a></li>
                        <li id="menuJellyfish"><a href="#">Jellyfish</a></li>
                        <li id="menuDesert"><a href="#">Desert</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--navbar-->

    </div>

    <!-- Main hero unit for a primary marketing message or call to action -->
    <div class="hero-unit">
        <h1>Mari Belajar Web Programming!</h1>

        <p>Kita belajar mendisain halaman dengan Bootstrap serta belajar action evennya dengan JavaScript dan
            JQuery.</p>

        <p><a role="button" data-toggle="modal" class="btn btn-primary btn-large">Tampilkan Modal &raquo;</a></p>
    </div>

    <hr>

    <div class="footer">
        <p>&copy; 2ndStack 2013</p>
    </div>

</div>
<!-- /container -->

<%--modal--%>
<div id="myModal" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Modal header</h3>
    </div>
    <div class="modal-body">
        <p>Modal tampil ....</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary">Save changes</button>
    </div>
</div>
<%--modal--%>

</body>
</html>