<!DOCTYPE html>
<%--
  Created by IntelliJ IDEA.
  User: Latief
  Date: 1/9/13
  Time: 10:09 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap.min.css">
    <script type="text/javascript" src="/resources/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/resources/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="/resources/bootstrap/js/bootstrap.min.js"></script>
    <title>Bootstrap Layout</title>
</head>
<body>

<%--navbar--%>
<div class="navbar navbar-inverse">
    <div class="navbar-inner">
        <div class="container">

            <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <!-- Be sure to leave the brand out there if you want it shown -->
            <a class="brand" href="#">Layout</a>

            <div class="nav-collapse collapse navbar-inverse-collapse">
                <ul class="nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">Link</a></li>
                </ul>
            </div>

        </div>
    </div>
</div>
<%--navbar--%>

<%--container--%>
<div class="container-fluid">

    <div class="row-fluid">

        <%--sidebar--%>
        <div class="span2" >

            <div class="well">
                <ul class="nav nav-list">
                    <li class="nav-header">Sidebar</li>
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">Library</a></li>
                </ul>
            </div>
        </div>
        <%--sidebar--%>

        <%--body--%>
        <div class="span10">
            <%--carousel--%>
            <div id="myCarousel" class="carousel slide">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="active item">
                        <img src="/resources/images/Desert.jpg"/>
                        <div class="carousel-caption">
                            <h4>Desert</h4>
                            <p>Gurun.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="/resources/images/Chrysanthemum.jpg"/>
                        <div class="carousel-caption">
                            <h4>Chrysanthemum</h4>
                            <p>Bunga Matahari.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="/resources/images/Jellyfish.jpg"/>
                        <div class="carousel-caption">
                            <h4>Jellyfish</h4>
                            <p>Ubur ubur.</p>
                        </div>
                    </div>
                </div>

                <!-- Carousel nav -->
                <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
            </div>
            <%--carousel--%>

        </div>
        <%--body--%>

    </div>
</div>
<%--container--%>

</body>
</html>