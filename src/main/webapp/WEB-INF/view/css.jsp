<%--
  Created by IntelliJ IDEA.
  User: Latief
  Date: 1/9/13
  Time: 10:09 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/resources/css/fam-icons.css">

    <title>CSS</title>
    <style type="text/css">
        p {
            color: red;
            text-align: center;
        }
        p.font {
            font-family: monospace;
        }
        span {
            font-size: 20px;
            color: white;
            background-color: black;
        }
        .merah {
            background-color: red;
            color: white;
        }
        #big {
            font-size: 100px;
        }
    </style>
</head>
<body>
<button><i class="fam-clock-pause"></i> Button with FamFamFam icons</button>
<h1 class="merah">Hello World!</h1>

<p class="font">This <span id="big">paragraph</span> is styled with <span>CSS</span>.</p>

<p class="merah">Ini paragraph 1</p>
</body>
</html>